#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import pandas as pd
import re
from time import sleep
import twitter
from tqdm import tqdm
from collections import namedtuple
from string import punctuation

Tweet = namedtuple("Tweet",("id", "date", "text", "kind", "keywords"))

def generate_with_prob(form, pat_list, pat_table, cell, dest):
    result = []
    total = 0
    form = form[0]
    for pattern in pat_list:
        others = pat_table[pat_table.apply(lambda x: pattern in x)]
        l = others.shape[0]
        total += l
        result.append({"generated": pattern.apply(form, (cell, dest)),
                       "P": 0,
                       "count": l,
                       "examples": ", ".join([str(x) for x in others.index]),
                       "pattern": pattern})

    result = pd.DataFrame(result)
    result["P"] = result["count"] / total
    result = result[result.P > 0].sort_values("P", ascending=False)
    return result


class TweetExtractor(object):

    def __init__(self, *args, **kwargs):
        self.api = twitter.Api(*args, **kwargs)
        self.tweets = set()

    def annotate_tweet(self, tweet_text, keywords):
        auxs = ["have", "has", "had", "is", "are", "was", "were", "be"]
        advs = ["not", "always", "often", "never", "ever", "sometimes", "even", "too",
                "well", "[^\s][^\s]+ly"]
        left_reg = fr"\s([^\s]+?)(?:n't)?\s+(?:{'|'.join(advs)})?[^\s]*({'|'.join(keywords)})"
        left_contexts = re.findall(left_reg, tweet_text)

        keywords = {w for left, w in left_contexts if left not in auxs}
        if len(keywords) == 0:
            return None

        quotes = "[«‹»›„“‟”`<>'’\"❝❞❮❯⹂〝〞〟＂‚‘‛❛❜❟]"
        reg = r".*({q}+\s*(?:{w})\s*{q}+|#(?:{w})|past of|past tense)".format(q=quotes,
                                                                              w="|".join(
                                                                                  keywords))
        mention_match = re.match(reg, tweet_text)

        return {"keywords": "+".join(sorted(keywords)),
                "kind": "mention" if mention_match or len(keywords) > 1 else "usage"}

    def search_twitter(self, query, keywords):
        def rows(tweets):
            for tweet in tweets:
                annotations = self.annotate_tweet(tweet.full_text, keywords)
                if annotations is not None:
                    yield Tweet(date=tweet.created_at,
                           id=tweet.id,
                           text=tweet.full_text,
                           **annotations)

        with tqdm(desc="Batches:") as pbar:
            results = self.api.GetSearch(term=query, lang="en", count="100",
                                         include_entities=True)
            min_id = min(r.id for r in results)
            prev_id = max(r.id for r in results)
            self.tweets = self.tweets.union(rows(results))
            pbar.update(1)
            while min_id < prev_id:
                try:
                    new_results = self.api.GetSearch(term=query, lang="en", count="100",
                                                     max_id=min_id,
                                                     include_entities=True)
                except (twitter.TwitterError, KeyboardInterrupt):
                    break
                prev_id = min_id
                min_id = min(r.id for r in new_results)
                pbar.update(1)
                for r in new_results:
                    if r.id == min_id:
                        pbar.set_description("Oldest: {}".format(r.created_at))

                self.tweets = self.tweets.union(rows(new_results))
